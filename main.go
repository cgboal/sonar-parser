package main

import (
	"bufio"
	"strings"
	"encoding/json"
	"os"
	"log"
	"regexp"
	"sync"
	"golang.org/x/sync/semaphore"
	"context"
	"runtime"
)

type SonarResult struct {
	Name string
}


func ParseFile(file *os.File, ch chan<- string) {
	var wg sync.WaitGroup
	lock := semaphore.NewWeighted(int64(runtime.NumCPU()) / 2)
	scanner := bufio.NewScanner(file)

	for scanner.Scan() {

		wg.Add(1)
		lock.Acquire(context.TODO(), 1)

		line := strings.TrimSpace(scanner.Text())
		go func (ch chan <- string) {
			defer wg.Done()
			defer lock.Release(1)

			var sonar_result SonarResult

			json.Unmarshal([]byte(line), &sonar_result)

			for _, tld := range GetTLDs() {
				var re = regexp.MustCompile(tld + "$")
				sonar_result_new := re.ReplaceAllString(sonar_result.Name, "")
				if sonar_result_new != sonar_result.Name {
					sonar_result.Name = sonar_result_new
					break
				}
			}
			sonar_name_parts := strings.Split(sonar_result.Name, ".")
			ch <- strings.Join(sonar_name_parts[:len(sonar_name_parts)-1], ".")
		} (ch)

	}
	wg.Wait()

	close(ch)
}


func Output(file *os.File, ch <-chan string) {
	for subdomain := range ch {
		file.WriteString(subdomain + "\n")
	}
}

func main() {

	filename := os.Args[1]
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}

	out_filename := os.Args[2]
	outfile, err := os.Create(out_filename)
	defer outfile.Close()
	if err != nil {
		log.Fatal(err)
	}

	subdomain_channel := make(chan string, 10)

	go Output(outfile, subdomain_channel)
	ParseFile(file, subdomain_channel)

	outfile.Sync()
}
